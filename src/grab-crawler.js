const wd = require('wd');
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
const moment = require('moment');
const settings = require('../configs/settings');

const ELEM_PICKUP_WIDGET_ID = 'com.grabtaxi.passenger:id/tv_poi_widget_pickup';
const ELEM_PICKUP_SEARCH_ID = 'com.grabtaxi.passenger:id/poi_first_search';
const ELEM_FIRST_STOP_SEARCH_ID = 'com.grabtaxi.passenger:id/poi_second_search';

// result after entering value to search box.
const ELEM_PICKUP_WIDGET_RESULT_ID = 'com.grabtaxi.passenger:id/vg_poi_item_content';

const ELEM_SERVICE_SELECTOR_ID = 'com.grabtaxi.passenger:id/selectedTaxiTypePickerView';

// values to get crawled data.
const ELEM_CURRENCY_ID = 'com.grabtaxi.passenger:id/currency';
const ELEM_PRICE_ID = 'com.grabtaxi.passenger:id/price';
const ELEM_PROMOTE_ID = 'com.grabtaxi.passenger:id/nbf_txt_promo';
const ELEM_PROMOTE_DEFAULT_VALUE = 'Promo';

const ELEM_POPUP_VIEW_ID = 'view';
const ELEM_POPUP_CLOSE_ID = 'close';

const ELEM_RESULT_POS = 0;

const MINIMAL_WAIT_MILLISECOND = settings.MINIMAL_WAIT_MILLISECOND;
const SCREENSHOTS_FOLDER_PATH = path.join(path.dirname(__dirname), 'screenshots');

function getCurrentTime(format) {
  if (format === 'unix') {
    return Date.now().toString();
  }

  return moment().format();
}

async function init(serverConfig, desiredCaps) {
  let driver = await wd.promiseChainRemote(serverConfig);
  await driver.init(desiredCaps).setImplicitWaitTimeout(MINIMAL_WAIT_MILLISECOND);
  if (driver) {
    console.log('Driver\'ve been initialized successfully!');
    return driver;
  } else {
    throw Error('Cannot initialize driver!');
  }
}

async function takeEntireScreenShoot(driver, folder, fileName) {
  let folderPath = path.join(SCREENSHOTS_FOLDER_PATH, folder);
  if (!fs.existsSync(folderPath)) {
    await mkdirp.sync(folderPath);
  }
  let filePath = path.join(folderPath, `${fileName}.png`);
  await driver.takeScreenshot().saveScreenshot(filePath);
}

async function closePopUp(driver) {
  try {
    let element = await driver.element('id', ELEM_POPUP_VIEW_ID);
    if (element && element.isDisplayed()) {
      await driver.element('id', ELEM_POPUP_CLOSE_ID).click();
      console.log('> Close popup successfully!');

      // remove the green popup
      await clickOnCoordinate(driver);
      return true;
    }
  } catch (error) {
    // console.log('> Error when closing popup:', error);
    console.log('> Dont have popup.');
    return true;
  }
}

function getFolderName(distance_id, category, vehicleType) {
  let newPath = path.join(distance_id, category, vehicleType);
  return path.join(newPath, getCurrentTime('unix'));
}

async function execute(from, to, distance_id, deviceId, category, vehicleType) {
  try {
    let folderName = getFolderName(distance_id, category, vehicleType);

    const serverConfig = settings.SERVER;
    let driver = await init(serverConfig, deviceId);
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('0. init successfully!');
    console.log('0. Current screen shoot folder: ', folderName);

    await closeDailyPopup(driver);
    let loginStatus = await doLogin(driver);

    if (loginStatus === null) {
      console.log('-1. Get error when trying to login.');
      return null
    }

    if (loginStatus) {
      console.log('0. Login successfully.');
    }

    let status = await goToCategory(driver, category);
    if (!status) {
      console.log('-1. Cannot go to category :', category);
      return null;
    }

    if (loginStatus === true) {
      let removeStatus = await removeGuideline(driver);
      if (removeStatus) {
        console.log('0. Remove all guideline successfully.');
      }
      // close any green popup remain.
      await clickOnCoordinate(driver);
    }

    if (vehicleType === settings.CAR_TYPE.CAR_4_SEATS.text) {
      return await crawlGrab4Seats(driver, folderName, from, to);
    }
  } catch (error) {
    console.log(error);
    return null;
  }
}

async function checkItemHasSubResult(driver) {
  try {
    let elements = await driver.elements('id', 'com.grabtaxi.passenger:id/poi_item_sub_container');

    // get first result and click:
    let elem = elements[0];
    await elem.click();
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    return true;
  } catch (e) {
    console.log("Don't have sub result");
    return false;
  }
}

async function crawlGrab4Seats(driver, folderName, from, to) {
  try {
    await takeEntireScreenShoot(driver, folderName, '0_init_completed');
    let isContinue = await closePopUp(driver);
    if (!isContinue) {
      console.log('0. Stopped due to unexpected error !');
      return -1;
    }
    // close any green popup remain.
    await clickOnCoordinate(driver);
    // select pickup place
    let pickupWidgetElem = await driver.element('id', ELEM_PICKUP_WIDGET_ID);
    await pickupWidgetElem.click();

    let pickupWidgetTextBoxElem = await driver.element('id', ELEM_PICKUP_SEARCH_ID);
    await pickupWidgetTextBoxElem.clear();
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    await pickupWidgetTextBoxElem.sendKeys(from);
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('1. send pickup value successfully!');
    await takeEntireScreenShoot(driver, folderName, '1_enter_pickup_completed');

    let pickupResultElements = await driver.elements('id', ELEM_PICKUP_WIDGET_RESULT_ID);
    console.log('2. Total pickup result: ', pickupResultElements.length);

    let firstPickupElement = pickupResultElements[ELEM_RESULT_POS];
    console.log('2.1 First pickup successfully');
    await takeEntireScreenShoot(driver, folderName, '2_choose_pickup_completed');

    let check = await checkItemHasSubResult(driver);
    if (check === false) {
      await firstPickupElement.click();
    }

    console.log('3. choose pickup place successfully!');
    await takeEntireScreenShoot(driver, folderName, '3_pickup_completed');

    // wait
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);

    // select drop off place
    let dropOffWidgetElement = await driver.element('id', ELEM_FIRST_STOP_SEARCH_ID);
    await dropOffWidgetElement.click();

    await dropOffWidgetElement.sendKeys(to);
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('4. send drop off value successfully!');
    await takeEntireScreenShoot(driver, folderName, '4_enter_drop_off_completed');

    let dropOffResultElements = await driver.elements('id', ELEM_PICKUP_WIDGET_RESULT_ID);
    console.log('5. Total drop off result: ', dropOffResultElements.length);

    let firstDropOffElement = dropOffResultElements[ELEM_RESULT_POS];
    console.log('5.1 First drop off result successfully!');
    await takeEntireScreenShoot(driver, folderName, '5_choose_drop_off_completed');

    // TODO handle the result if there are more than one result.
    // let check = await checkItemHasSubResult(driver);
    // if (check === false) {
    //  await firstPickupElement.click();    
    // }
    // END TODO 
    await firstDropOffElement.click();
    console.log('6. choose drop off place successfully!');
    await takeEntireScreenShoot(driver, folderName, '6_drop_off_completed');

    // wait
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);

    // get promote code
    let promoteCodeElem = await driver.element('id', ELEM_PROMOTE_ID);
    let promoteCode = await promoteCodeElem.getAttribute('text');
    if (promoteCode === ELEM_PROMOTE_DEFAULT_VALUE) {
      promoteCode = '';
    }

    // select 4-seat taxi service
    let serviceSelectorElem = await driver.element('id', ELEM_SERVICE_SELECTOR_ID);
    await serviceSelectorElem.click();
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('7.0 Click to service selector successfully!');
    await takeEntireScreenShoot(driver, folderName, '7_select_service_completed');

    // get price
    let currencyElements = await driver.elements('id', ELEM_CURRENCY_ID);
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('7.1 Total service currency: ', currencyElements.length);
    let priceElements = await driver.elements('id', ELEM_PRICE_ID);
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    console.log('7.2 Total service price: ', priceElements.length);

    let firstCurrency = currencyElements[ELEM_RESULT_POS];
    console.log('7.3 Get first currency successfully!');

    let firstPrice = priceElements[ELEM_RESULT_POS];
    console.log('7.4 Get first price successfully!');

    let currencyValue = await firstCurrency.getAttribute('text');
    let priceValue = await firstPrice.getAttribute('text');

    let bookingPriceValue = priceValue; // TODO
    let fareType = 'normal';// TODO
    let membership = 'member'; // TODO
    let payment = 'cash';// TODO
    let density = 0; // TODO

    let crawledResult = {
      from: from,
      to: to,
      price: priceValue,
      currency: currencyValue,
      promote_code: promoteCode,
      booking_price: bookingPriceValue,
      crawling_time: getCurrentTime(),
      available_car: 0,
      payment: payment,
      membership: membership,
      fare_type: fareType,
      vehicle_type: settings.CAR_TYPE.CAR_4_SEATS.text,
      density: density
    };

    console.log("RESULT: ", crawledResult);
    return crawledResult;
  } catch (error) {
    console.log("ERROR - crawlGrab4Seats -", error);
    return null;
  }
}

///////////// GRAB 4 PLACE CATEGORY FROM HOME
const MAIN_PAGE_CATEGORY_ID = 'com.grabtaxi.passenger:id/tile_view';
const CAR_CATEGORY_POSITION = 0;
async function goToCategory(driver, category) {
  try {
    if (category === settings.CATEGORIES.CAR) {
      let categoryElems = await driver.elements('id', MAIN_PAGE_CATEGORY_ID);
      let carCategoryButton = categoryElems[CAR_CATEGORY_POSITION];
      await carCategoryButton.click();
      await driver.sleep(MINIMAL_WAIT_MILLISECOND);
      return true;
    } else {
      return null;
    }
  } catch (err) {
    console.log('[goToCategory] error: ', err);
    return false;
  }
}

////////////// CLOSE DAILY POPUP
const DAILY_POPUP_CLOSE_ID = 'close-button';
async function closeDailyPopup(driver) {
  try {
    const foundElement = await driver.element('id', DAILY_POPUP_CLOSE_ID);
    await foundElement.click();
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);
    return true;
  } catch (err) {
    console.log('[closeDailyPopup] Dont have daily popup!');
    return false;
  }
}

////////////// LOGIN USING GOOGLE ACCOUNT
const GOOGLE_LOGIN_BUTTON_ID = 'com.grabtaxi.passenger:id/on_boarding_google_login_button';
const GOOGLE_ACC_NAME_ID = 'com.google.android.gms:id/account_display_name';
async function doLogin(driver) {
  try {
    const foundElement = await driver.element('id', GOOGLE_LOGIN_BUTTON_ID);
    await foundElement.click();
    await driver.sleep(MINIMAL_WAIT_MILLISECOND);

    // check if there are a list of google account
    // if yes, get the FIRST account to login,
    // if not, do manually login using account info from database.
    try {
      const accountElements = await driver.elements('id', GOOGLE_ACC_NAME_ID);
      if (accountElements.length > 0) {
        // click on first account
        // every device must login with only one GG account.
        let firstAccountElement = accountElements[0];
        await firstAccountElement.click();
        await driver.sleep(MINIMAL_WAIT_MILLISECOND);
        return true;
      }
      return null;
    } catch (err) {
      console.log('[doLogin] Dont have list account to select.. ');
      return null;
    }
  } catch (err) {
    console.log('[doLogin] Dont need to login.');
    return false;
  }
}

////////////// REMOVE GUIDELINE WHEN FIRST LOGIN
// const CLOSE_NAV_CLOSE_BUTTON_ID = 'com.grabtaxi.passenger:id/navTooltipCloseBtn';
const TOOLTIP_CIRCLE_ID = 'com.grabtaxi.passenger:id/navServiceSelectorTooltipCircle';
const BACK_BUTTON_ID = 'Back';
const MINIMAL_WAIT_PREPARING_MILLISECOND = 2000;
async function removeGuideline(driver) {
  try {
    const circleElement1 = await driver.element('id', TOOLTIP_CIRCLE_ID);
    await circleElement1.click();
    await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);

    let dropOffResultElements = await driver.elements('id', ELEM_PICKUP_WIDGET_RESULT_ID);
    let firstDropOffElement = dropOffResultElements[ELEM_RESULT_POS];
    await firstDropOffElement.click();
    await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);

    try {
      const circleElement2 = await driver.element('id', TOOLTIP_CIRCLE_ID);
      await circleElement2.click();
      await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);
    } catch(err) {
      await clickOnCoordinate(driver);
      await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);

      const circleElement2 = await driver.element('id', TOOLTIP_CIRCLE_ID);
      await circleElement2.click();
      await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);
    }

    await driver.back();

    // remove the green popup
    await clickOnCoordinate(driver);

    await driver.back();

    // remove the green popup
    await clickOnCoordinate(driver);

    await driver.sleep(MINIMAL_WAIT_PREPARING_MILLISECOND);
    return true;
  } catch (err) {
    console.log('[removeGuideline] Dont have tooltip near pickup place!');
    return false;
  }
}

async function clickOnCoordinate(driver, _x = null, _y = null) {
  if (_x === null && _y === null) {
    let handleOneSize = await driver.getWindowSize();

    _x = handleOneSize.width / 2;
    _y = handleOneSize.height / 10 - 2;
  }

  await (new wd.TouchAction(driver))
    .tap({x: _x, y: _y})
    .perform();
  return true;
}

module.exports = execute;