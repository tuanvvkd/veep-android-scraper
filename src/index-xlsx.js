const moment = require('moment');

const xlsx = require('./xlsx');
const grabCrawler = require('./grab-crawler');
const stringUtils = require('./utils/stringUtils');
const settings = require('../configs/settings');

function genRandomNumber(max) {
  return Math.floor((Math.random() + 1) * max);
}

async function execute() {
  let inputData = await xlsx.readData(settings.INPUT_XLSX_FILE, settings.INPUT_SHEET_NAME_AUTO);
  // console.log('> Input data: ', inputData);

  let outputData = [];
  let totalInput = inputData.length;
  if (totalInput > 0) {
    for (let i = 0; i < totalInput; i++) {
      let data = inputData[i];
      let result = Object.assign({}, data);
      console.log(`>>>>>>>>>>> Begin crawl data from=${data.from}, to=${data.to}:`);

      let newFrom = stringUtils.convertToNonUnicode(data.from);
      let newTo = stringUtils.convertToNonUnicode(data.to);
      let crawledResult = await grabCrawler(newFrom, newTo);

      result.book_price = crawledResult['price'].replace('K', '000');
      result.book_time = crawledResult['crawling_time'];
      result.from_result = crawledResult['from_result'];
      result.to_result = crawledResult['to_result'];
      result.from_keyword = crawledResult['from'];
      result.to_keyword = crawledResult['to'];

      await xlsx.writeData(result, settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);
      outputData.push(result);
      let second = genRandomNumber(20);
      console.log(`>>>>>>>>>>> Wait for ${second} seconds....`);
      if (i < totalInput - 1) {
        await waitForMilisecond(second * 1000);
      }
    }
  }


  // testing
  // let result = Object.assign({}, inputData[0]);
  // result.book_price = '25000';
  // result.book_time = moment().format();
  // result.from_result = '294 Trường Sa';
  // result.to_result = 'Dinh độc lập';
  // result.from_keyword = '294 Truong Sa';
  // result.to_keyword = 'Dinh Doc Lap';
  // console.log('result: ', result);
  //
  // await xlsx.writeData(result, settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);
  // await xlsx.writeData(result, settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);
  // await xlsx.writeData(result, settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);
  // await xlsx.writeData(result, settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);

  console.log('> Output data: ', outputData);
}

async function waitForMilisecond(value) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(true);
    }, value, `Done for waiting ${value} seconds.`);
  });
}

execute();