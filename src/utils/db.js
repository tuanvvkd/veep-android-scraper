const MongoClient = require('mongodb').MongoClient;
const settings = require('../../configs/settings');
class DatabaseClient {
  constructor(_conStr, _dbName) {
    this.connectionString = _conStr;
    this.dbName = _dbName;
  }

  async connectToDb() {
    if (this.db === undefined) {
      this.client = new MongoClient(this.connectionString);
      await this.client.connect();
      this.db = this.client.db(this.dbName);
    }
  }
}
module.exports.readData = readAllData;
module.exports.writeData = writeData;

let connectionStr = null;
if (process.env.NODE_ENV === 'development') {
  connectionStr = settings.DB_CONNECTION_STRING_DEV;
}
else {
  connectionStr = settings.DB_CONNECTION_STRING_PROD;
}
const dbClient = new DatabaseClient(connectionStr, settings.DB_NAME);

async function readAllData(collectionName, query = {}, options = {}) {
  await dbClient.connectToDb();
  let data = await dbClient.db.collection(collectionName).find(query, options).toArray();
  if (data.length > 0) {
    return data.map(x => {
      delete x.createdAt;
      delete x.updatedAt;
      delete x._id;
      delete x.__v;
      return x;
    })
  }
  else {
    return [];
  }
}

async function writeData(collectionName, data) {
  await dbClient.connectToDb();
  let inputData = [];
  if (!Array.isArray(data)) {
    inputData = inputData.concat([data]);
  }
  else {
    inputData = inputData.concat(data);
  }

  let totalInserted = 0;
  let totalInput = inputData.length;
  if (totalInput > 0) {
    for (let i = 0; i < totalInput; i++) {
      let data = inputData[i];
      data.createdAt = new Date();
      data.updatedAt = new Date();
      delete data._id;

      await dbClient.db.collection(collectionName).insertOne(data);
      totalInserted += 1;
    }
  }
  return totalInserted === totalInput;
}
