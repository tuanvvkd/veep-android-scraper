const diacritics = require('diacritics');
module.exports.convertToNonUnicode = (strValue) => {
  return diacritics.remove(strValue);
};