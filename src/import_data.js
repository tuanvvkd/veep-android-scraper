const xlsx = require('./xlsx');
const MongoClient = require('mongodb').MongoClient;
const settings = require('../configs/settings');

async function connectDB() {
  const client = new MongoClient(settings.DB_CONNECTION_STRING);
  await client.connect();
  return client.db(settings.DB_NAME);
}


async function execute() {
  try {
    let inputData = await xlsx.readCrawledData(settings.INPUT_XLSX_FILE, settings.OUTPUT_SHEET_NAME);
    let db = await connectDB();
    let collection = db.collection(settings.DB_RECORD_COLLECTION);
    
    let totalInput = inputData.length;
    if (totalInput > 0) {
      for (let i = 0; i < totalInput; i++) {
        let data = inputData[i];
        if (data.city === 'HCM') {
          data.city = 'Hồ Chí Minh';
        }
        data.district = data.district.toString();
        data.book_time = new Date(data.book_time);
        data.createdAt = new Date();
        data.updatedAt = new Date();

        let iResult = await collection.insert(data);
        console.log(`imported ${iResult.result.n} document.`);
      }
    }

    console.log('Import data successfully!');
  }
  catch (err) {
    console.log('Error: ', err);
  }
}


execute();

