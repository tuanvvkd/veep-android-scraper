module.exports = Object.freeze({

  INPUT_SHEET_NAME: 'Input template',
  INPUT_SHEET_NAME_AUTO: 'Input template auto',
  INPUT_SHEET_NAME_TEST: 'Input template testing',

  SERVER: {
    host: '0.0.0.0',
    port: 4723
  },

  CATEGORIES: {
    CAR: 'Car',
    BIKE : 'Bike',
    FOOD : 'Food',
    DELIVERY: 'Delivery'
  },

  CAR_TYPE: {
    CAR_4_SEATS : {
      text: 'GrabCar 4 chỗ',
      pos: 1
    }
  },

  OUTPUT_SHEET_NAME: 'Output template',

  DB_CONNECTION_STRING_DEV: 'mongodb://localhost:27017/grab-scraper',
  DB_CONNECTION_STRING_PROD: 'mongodb://grabAdmin:sg5t5uXZanVGUJ7V@45.122.223.197:7998/grab-scraper',

  DB_NAME: 'grab-scraper',
  DB_RECORD_COLLECTION: 'records',
  DB_DISTANCE_COLLECTION: 'distances',

  AW_LOCATION_KEY_HCM: '353981', // HO CHI MINH
  AW_APP_NAME: 'grab-crawler',
  AWS_KEY : [
    'nNrCGHaj39vQ25rbKj8kfe9YTxUMOBOV',
    'Ifq9wPFTIFylqTHDSC2kMOVwBYAXaxEN'
  ],
  AW_LANGUAGE: 'vi',

  MINIMAL_WAIT_MILLISECOND: 5000,

  DEVICE_01: {
    "platformName": "Android",
    "platformVersion": "8.1",
    "deviceName": "DevDev",
    "appPackage": "com.grabtaxi.passenger",
    "appActivity": "com.grab.pax.newface.presentation.newface.NewFace",
    "noReset": true
  },

  DEVICE_02: {
    platformName: 'Android',
    platformVersion: '7.0',
    deviceName: 'Android Device',
    appPackage: 'com.grabtaxi.passenger',
    appActivity: 'com.grab.pax.newface.presentation.newface.NewFace',
    noReset: true,
    unicodeKeyboard: false
  },

  DEVICE_03: {
    platformName: 'Android',
    platformVersion: '5.0.2',
    deviceName: 'Galaxy Tab S2',
    appPackage: 'com.grabtaxi.passenger',
    appActivity: 'com.grab.pax.newface.presentation.newface.NewFace',
    noReset: true,
    unicodeKeyboard: false
  },

  DEVICE_04: {
    platformName: 'Android',
    platformVersion: '7.0',
    deviceName: 'Galaxy S6 Edge+',
    appPackage: 'com.grabtaxi.passenger',
    appActivity: 'com.grab.pax.newface.presentation.newface.NewFace',
    noReset: true,
    unicodeKeyboard: false
  }
});